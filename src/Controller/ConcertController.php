<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Event;
use App\forms\EventType;
use App\Entity\Artiste;
use App\forms\ArtisteType;
use App\Entity\City;
use App\forms\CityType;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;
use DateTime;

class ConcertController extends Controller
{
    /**
     * @Route("/", name="concert")
     */
    public function index(Request $request)
    {
        $cities = $this->getDoctrine()->getRepository('App:City')->findAll();
        $artistes = $this->getDoctrine()->getRepository('App:Artiste')->findAll();
        $dateMin = new DateTime();
        $dateMax = new DateTime();

        if(
            $request->get('dateMin') != null
            || $request->get('dateMax') != null
            || $request->get('artiste') != null
            || $request->get('city') != null)
        {
            $dateMin = $request->get('dateMin');
            $dateMax = $request->get('dateMax');
            $search = [
                'dateMin' => $dateMin,
                'dateMax' => $dateMax,
                'artiste' => $this->getDoctrine()
                    ->getRepository('App:Artiste')
                    ->findOneByName($request->get('artiste')),
                'city' => $this->getDoctrine()
                    ->getRepository('App:City')
                    ->findOneByName($request->get('city'))
            ];

            $events = $this->getDoctrine()->getRepository('App:Event')->findEvent($search);

        }
        else{
            $events = $this->getDoctrine()->getRepository('App:Event')->findByDeleted(0);
        }

        $paginator  = $this->get('knp_paginator');
        $events = $paginator->paginate(
            $events,
            $request->query->getInt('page', 1),
            10/*limit per page*/
        );
        return $this->render('concert/index.html.twig', [
            'cities' => $cities,
            'artistes' => $artistes,
            'events' => $events,
            'dateMin' => $dateMin,
            'dateMax' => $dateMax,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete($id)
    {

        $event = $this->getDoctrine()->getRepository('App:Event')->find($id);
        $event->setDeleted(1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($event);
        $em->flush();

        return $this->redirectToRoute('concertAdmin');
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request, $id)
    {
        $cities = $this->getDoctrine()->getRepository('App:City')->findAll();
        $artistes = $this->getDoctrine()->getRepository('App:Artiste')->findAll();
        $event = $this->getDoctrine()->getRepository('App:Event')->find($id);
        $event->setDeleted(1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($event);
        $em->flush();
        if($request->get('date') != null){

            $time = $request->get('date').' '.$request->get('heure').':00';
            $date = new \DateTime($time);

            $artiste = $this->getDoctrine()->getRepository('App:Artiste')->findOneByName($request->get('artiste'));
            $city = $this->getDoctrine()->getRepository('App:City')->findOneByName($request->get('city'));

            $event = new Event();
            $event
                ->setCity($city)
                ->setArtiste($artiste)
                ->setDate($date)
                ->setDeleted(false)
            ;

            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('concertAdmin');
        }


        return $this->render('concert/editEvent.html.twig', [
            'event' => $event,
            'cities' => $cities,
            'artistes' => $artistes
        ]);
    }

    /**
     * @Route("/administration/concert", name="concertAdmin")
     */
    public function events(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $cities = $this->getDoctrine()->getRepository('App:City')->findAll();
        $artistes = $this->getDoctrine()->getRepository('App:Artiste')->findAll();

        if($request->get('date') != null){
            $time = $request->get('date').' '.$request->get('heure').':00';
            $date = new \DateTime($time);

            $artiste = $this->getDoctrine()->getRepository('App:Artiste')->findOneByName($request->get('artiste'));
            $city = $this->getDoctrine()->getRepository('App:City')->findOneByName($request->get('city'));

            $event = new Event();
            $event
                ->setCity($city)
                ->setArtiste($artiste)
                ->setDate($date)
                ->setDeleted(false)
            ;

            $em->persist($event);
            $em->flush();

        }
        $events = $this->getDoctrine()->getRepository('App:Event')->findByDeleted(0);

        $paginator  = $this->get('knp_paginator');
        $events = $paginator->paginate(
            $events,
            $request->query->getInt('page', 1),
            10/*limit per page*/
        );
        return $this->render('concert/administationConcerts.html.twig', [
            'cities' => $cities,
            'artistes' => $artistes,
            'events' => $events
        ]);
    }

    /**
     * @Route("/administration/artistes", name="concertAdminArtiste")
     */
    public function admin(Request $request)
    {



        //admin artiste
        $artistes = $this->getDoctrine()->getRepository('App:Artiste')->findAll();
        $artiste = new Artiste();
        $form = $this->createForm(ArtisteType::class, $artiste);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $event = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();
            return $this->redirectToRoute('concertAdminArtiste');
        }

        //admin city
        $cities = $this->getDoctrine()->getRepository('App:City')->findAll();
        $city = new City();
        $form2 = $this->createForm(CityType::class, $city);
        $form2->handleRequest($request);
        if ($form2->isSubmitted() && $form2->isValid()) {
            $city = $form2->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($city);
            $entityManager->flush();
            return $this->redirectToRoute('concertAdminArtiste');
        }


        return $this->render('concert/adminArtistesVilles.html.twig', array(
            'form' => $form->createView(),
            'form2' => $form2->createView(),
            'artistes' => $artistes,
            'cities' => $cities
        ));

    }
}
