<?php
/**
 * Created by PhpStorm.
 * User: pmw8
 * Date: 24/10/2018
 * Time: 12:09
 */

namespace App\forms;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EventType extends AbstractType
{

    public function buildForm( FormBuilderInterface $builder, array $options){
        $builder
            ->add('event')
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
                'label'  => 'Date du concert',
            ])
            ->add('save', SubmitType::class);
    }
}