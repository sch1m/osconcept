<?php
/**
 * Created by PhpStorm.
 * User: pmw8
 * Date: 24/10/2018
 * Time: 12:17
 */

namespace App\forms;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;

class ArtisteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('save', SubmitType::class,[
                'label'  => "Enregistrer l'artiste",
            ]);
    }

}