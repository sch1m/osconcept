<?php

namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * @return Event[] Returns an array of Event objects
     */

    public function findEvent($search)
    {
        $dateMin = $search['dateMin'];
        $dateMax = $search['dateMax'];
        $artiste = $search['artiste'];
        $city = $search['city'];
        if($artiste!=null){
            return $this->createQueryBuilder('e')
                ->andwhere('e.date BETWEEN :dateMin AND :dateMax')
                ->andWhere('e.artiste = :artiste')
                ->andWhere('e.city = :city')
                ->andWhere('e.deleted = 0')
                ->setParameters([
                    'dateMin' => $dateMin,
                    'dateMax' => $dateMax,
                    'artiste' => $artiste,
                    'city' => $city,
                ])
                ->orderBy('e.id', 'ASC')
                ->getQuery()
                ->getResult()
                ;
        }
        else{

            return $this->createQueryBuilder('e')
                ->andwhere('e.date BETWEEN :dateMin AND :dateMax')
                ->andWhere('e.city = :city')
                ->andWhere('e.deleted = 0')
                ->setParameters([
                    'dateMin' => $dateMin,
                    'dateMax' => $dateMax.' 23:59:59',
                    'city' => $city,
                ])
                ->orderBy('e.id', 'ASC')
                ->getQuery()
                ->getResult()
                ;
        }


    }


    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
